<?xml version="1.0" encoding="UTF-8"?>
<schema targetNamespace="http://www.uni-stuttgart.de/iaas/ipsm/v0.5/"
        elementFormDefault="qualified" xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:pref="http://docs.oasis-open.org/tosca/ns/2011/12" xmlns:ipsm="http://www.uni-stuttgart.de/iaas/ipsm/v0.5/">

    <import schemaLocation="TOSCA-v1.0.xsd"
            namespace="http://docs.oasis-open.org/tosca/ns/2011/12"></import>
    <complexType name="tEntityDefinition">
        <annotation>
            <documentation>
                A generic way to describe entities
            </documentation>
        </annotation>
        <sequence>
            <element name="DefinitionContent" type="ipsm:tContent" minOccurs="1" maxOccurs="1"></element>
            <element name="Properties" type="ipsm:tProperties" minOccurs="0" maxOccurs="1"></element>
        </sequence>
        <attribute name="definitionLanguage" type="anyURI"></attribute>
    </complexType>

    <complexType name="tEntityDefinitions">
        <sequence>
            <element name="EntityDefinition" type="ipsm:tEntityDefinition"
                     minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tEntityIdentity">
        <annotation>
            <documentation>
                An identifier for entities
            </documentation>
        </annotation>
        <attribute name="name" type="string" use="required"></attribute>
        <attribute name="targetNamespace" type="string" use="required"></attribute>
    </complexType>


    <complexType name="tIdentifiableEntityDefinition">
        <annotation>
            <documentation>
                Each identifiable entity definition is identified
                using a name and
                namespace pair.
                Moreover they can specify multiple
                entity definitions.
            </documentation>
        </annotation>
        <sequence>
            <any namespace="##other" processContents="lax" minOccurs="0"
                 maxOccurs="unbounded" />
            <element name="EntityIdentity" type="ipsm:tEntityIdentity" minOccurs="1" maxOccurs="1"></element>
            <element name="EntityDefinitions" type="ipsm:tEntityDefinitions"
                     minOccurs="1" maxOccurs="1"></element>
        </sequence>
    </complexType>
    <element name="InstanceDescriptors" type="ipsm:tInstanceDescriptors"></element>
    <complexType name="tInstanceDescriptors">
        <sequence>
            <element name="InstanceDescriptor" type="ipsm:tInstanceDescriptor"
                     minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tInitializableEntityDefinition">
        <annotation>
            <documentation>
                Each acquirable / initializable entity definition.
            </documentation>
        </annotation>
        <sequence>
            <element name="IdentifiableEntityDefinition" type="ipsm:tIdentifiableEntityDefinition"
                     minOccurs="1" maxOccurs="1"></element>
            <element name="InstanceDescriptors" type="ipsm:tInstanceDescriptors"
                     minOccurs="0" maxOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tInteractiveEntityDefinition">
        <sequence>
            <element name="IdentifiableEntityDefinition" type="ipsm:tIdentifiableEntityDefinition" minOccurs="1" maxOccurs="1"></element>
            <element name="ParticipantList" type="ipsm:tParticipantList" maxOccurs="1" minOccurs="0"></element>
        </sequence>
    </complexType>

    <complexType name="tInteractiveInitializableEntityDefinition">
        <sequence>
            <element name="InitializableEntityDefinition" type="ipsm:tInitializableEntityDefinition" minOccurs="1" maxOccurs="1"></element>
            <element name="InteractiveEntityDefinition" type="ipsm:tInteractiveEntityDefinition" minOccurs="0" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>


    <complexType name="tStrategyRefs">
        <sequence>
            <element name="Strategy" type="QName" maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tRelatedIntentions">
        <sequence>
            <element name="RelatedIntention" type="string" maxOccurs="unbounded" minOccurs="0"></element>
        </sequence>
        <attribute name="relationType" type="string" use="required"></attribute>
    </complexType>

    <simpleType name="tPriority">
        <restriction base="integer">
            <minInclusive value="0"></minInclusive>
            <maxInclusive value="10"></maxInclusive>
        </restriction>
    </simpleType>

    <complexType name="tIntentionDefinition">
        <sequence>
            <element name="InteractiveInitializableEntityDefinition" type="ipsm:tInteractiveInitializableEntityDefinition"  maxOccurs="1" minOccurs="1"></element>
            <element name="RelatedIntentions" type="ipsm:tRelatedIntentions" maxOccurs="unbounded" minOccurs="0"></element>
            <element name="AchievingStategies" type="ipsm:tStrategyRefs" maxOccurs="unbounded" minOccurs="0"></element>
            <element name="InitialContext" type="QName" maxOccurs="1" minOccurs="0"></element>
            <element name="FinalContext" type="QName" maxOccurs="1" minOccurs="0"></element>
            <element name="OrganizationalCapabilities" type="ipsm:tOrganizationalCapabilityRefs" maxOccurs="1" minOccurs="0"></element>
        </sequence>
        <attribute name="priority" type="ipsm:tPriority"></attribute>
        <attribute name="due" type="dateTime"></attribute>
    </complexType>

    <complexType name="tResourceReferences">
        <sequence>
            <element name="ProvidingResource" type="QName">
            </element>
        </sequence>
    </complexType>

    <complexType name="tCapabilityProperties">
        <sequence>
            <element name="ProvidingResourceDefinition" type="ipsm:tResourceReferences"></element>
            <element name="DesiredResourceDefinition" type="QName"></element>
        </sequence>
    </complexType>

    <complexType name="tIntentionDefinitions">
        <sequence>
            <element name="IntentionDefinition" type="ipsm:tIntentionDefinition" maxOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tCapabilityDefinitions">
        <sequence>
            <element name="CapabilityDefinition" type="ipsm:tCapabilityDefinition" maxOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tOrganizationalDefinitions">
        <complexContent>
            <extension base="ipsm:tIdentifiableEntityDefinition">
                <sequence>
                    <element name="OrganizationalIntentions" type="ipsm:tIntentionDefinitions"
                             maxOccurs="1" minOccurs="0">
                    </element>
                    <element name="OrganizationallResources" type="pref:tDefinitions"
                             maxOccurs="1" minOccurs="0">
                    </element>
                    <element name="ResourceDrivenProcesses" type="ipsm:tResourceDrivenProcessDefinitions"
                             maxOccurs="1" minOccurs="0">
                    </element>
                    <element name="OrganizationalCapabilities" type="ipsm:tCapabilityDefinitions"
                             maxOccurs="1" minOccurs="0"></element>
                    <element name="OrganizationalContexts" type="ipsm:tContextDefinitions" maxOccurs="1" minOccurs="0"></element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>


    <complexType name="tDeploymentDescriptor">
        <attribute name="runnableUri" type="anyURI"></attribute>
        <attribute name="executionEnvironment" type="anyURI"></attribute>
        <attribute name="runnableName" type="string"></attribute>
    </complexType>


    <complexType name="tInstanceDescriptor">
        <sequence>
            <element name="IdentifiableEntityDefinition" type="ipsm:tIdentifiableEntityDefinition"/>
            <element name="sourceModel" type="ipsm:tEntityIdentity"></element>
            <element name="parentInstance" type="ipsm:tEntityIdentity"></element>
        </sequence>
        <attribute name="startTime" type="string" use="optional"></attribute>
        <attribute name="endTime" type="string" use="optional"></attribute>
        <attribute name="instanceState" type="string" use="required"></attribute>
        <attribute name="instanceUri" type="anyURI" use="optional"></attribute>
        <attribute name="propogateSuccess" type="boolean" use="optional"></attribute>
        <attribute name="propogateFailure" type="boolean" use="optional"></attribute>
        <attribute name="importance" type="int" use="optional"></attribute>
        <anyAttribute></anyAttribute>
    </complexType>

    <complexType name="tProperties">
        <sequence>
            <any minOccurs="1" maxOccurs="unbounded"></any>
        </sequence>
    </complexType>

    <complexType name="tStructuredProperties">
        <sequence>
            <any></any>
        </sequence>
    </complexType>

    <complexType name="tIntentionResourceCapabilityRef">
        <sequence>
            <element name="Intention" type="QName" maxOccurs="1"
                     minOccurs="1"></element>
            <element name="OrganizationalCapability" type="QName" maxOccurs="1" minOccurs="0"></element>
            <element name="OrganizationalResource" type="QName" maxOccurs="1" minOccurs="0"></element>
        </sequence>
    </complexType>

    <complexType name="tIntentionResourceCapabilityRefs">
        <sequence>
            <element name="IntentionResourceCapability" type="ipsm:tIntentionResourceCapabilityRef" maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tIntentionRefs">
        <sequence>
            <element name="Intention" type="QName" maxOccurs="unbounded"
                     minOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tContent">
        <sequence>
            <any></any>
        </sequence>
    </complexType>

    <complexType name="tContextDefinition">
        <sequence>
            <element name="InteractiveEntityDefinition" type="ipsm:tInteractiveEntityDefinition"  maxOccurs="1" minOccurs="1"></element>
            <element name="ContainedContexts" type="ipsm:tContextDefinitionRefs" maxOccurs="1" minOccurs="0"></element>
        </sequence>
    </complexType>

    <complexType name="tContextDefinitionRefs">
        <sequence>
            <element name="ContextDefinition" type="QName" maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>

    <element name="ContextDefinitions" type="ipsm:tContextDefinitions"></element>
    <complexType name="tContextDefinitions">
        <sequence>
            <element name="ContextDefinition" type="ipsm:tContextDefinition" maxOccurs="unbounded"
                     minOccurs="1"></element>
        </sequence>
    </complexType>

    <element name="ContextSet" type="ipsm:tContextDefinitionRefs"></element>

    <complexType name="tOrganizationalProcesses">
        <sequence>
            <element name="ResourceDrivenProcessDefinition" type="ipsm:tResourceDrivenProcessDefinition"></element>
        </sequence>
    </complexType>

    <element name="OrganizationalDefinitions" type="ipsm:tOrganizationalDefinitions">    </element>

    <complexType name="tFunctionalCapabilityRefs">
        <sequence>
            <element name="FunctionalCapability" type="QName"></element>
        </sequence>
    </complexType>

    <complexType name="tOrganizationalCapabilityRefs">
        <sequence>
            <element name="OrganizationalCapability" type="QName"
                     maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>


    <complexType name="tCapabilityDefinition">
        <sequence>
            <element name="InteractiveInitializableEntityDefinition" type="ipsm:tInteractiveInitializableEntityDefinition"  maxOccurs="1" minOccurs="1"></element>
            <element name="ProvidingResources" type="ipsm:tResourceRefs" minOccurs="0" maxOccurs="1"></element>
            <element name="DesiredResources" type="ipsm:tResourceRefs" minOccurs="0" maxOccurs="1"></element>
            <element name="ComposingCapabilities" type="ipsm:tCapabilityDefinition" minOccurs="0" maxOccurs="1"></element>
            <element name="CapabilityType" type="pref:tCapabilityType" minOccurs="1" maxOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tResourceRefs">
        <sequence>
            <element name="ResourceRef" type="QName" minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tResourceModel">
        <sequence>
            <element name="Definitions" type="pref:tDefinitions"
                     minOccurs="0" maxOccurs="1"></element>
        </sequence>
        <attribute name="ref" type="QName"></attribute>
        <attribute name="resourceModelUri" type="anyURI"></attribute>
    </complexType>

    <element name="ResourceDrivenProcessDefinition" type="ipsm:tResourceDrivenProcessDefinition"></element>

    <complexType name="tResourceDrivenProcessDefinition">
        <sequence>
            <element name="InteractiveInitializableEntityDefinition" type="ipsm:tInteractiveInitializableEntityDefinition"  maxOccurs="1" minOccurs="1"></element>
            <element name="TargetIntentions" type="ipsm:tIntentionRefs" maxOccurs="1" minOccurs="1"></element>
            <element name="ResourceModel" type="ipsm:tResourceModel">
            </element>
        </sequence>
    </complexType>


    <element name="InstanceDescriptor">
        <complexType>
            <complexContent>
                <extension base="ipsm:tInstanceDescriptor" />
            </complexContent>
        </complexType>
    </element>

    <complexType name="tPreconditionerRelationships">
        <sequence>
            <element name="Relationship" type="pref:tRelationshipTemplate"
                     maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tDependencies">
        <sequence>
            <element name="Dependency" type="pref:tNodeTemplate"
                     maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>


    <complexType name="tOperationMessage">
        <sequence>
            <element name="PreconditionerRelationships" type="ipsm:tPreconditionerRelationships"
                     maxOccurs="1" minOccurs="0">
            </element>
            <element name="Dependencies" type="ipsm:tDependencies"
                     maxOccurs="1" minOccurs="0">
            </element>
            <element name="InstanceUri" type="anyURI" maxOccurs="1"
                     minOccurs="0">
            </element>
            <element name="ErrorDefinition" type="string" maxOccurs="1"
                     minOccurs="0">
            </element>
            <element name="InstanceState" type="string" maxOccurs="1"
                     minOccurs="0">
            </element>
            <element name="TargetResource" type="pref:tNodeTemplate"
                     maxOccurs="1" minOccurs="1">
            </element>
            <element name="ResourceDrivenProcessDefinitions" type="ipsm:tResourceDrivenProcessDefinition"
                     maxOccurs="1" minOccurs="0">
            </element>

            <element name="OtherParameters" type="ipsm:tOtherParameters"
                     maxOccurs="1" minOccurs="0"></element>
        </sequence>
        <attribute name="processId" type="string"></attribute>
        <attribute name="operationType" type="string"></attribute>
    </complexType>

    <element name="OperationMessage" type="ipsm:tOperationMessage">
    </element>

    <complexType name="tOtherParameters">
        <sequence>
            <any></any>
        </sequence>
    </complexType>


    <complexType name="tParticipant">
        <attribute name="participantRef" type="string" use="required"></attribute>
        <attribute name="participantType" type="string" use="optional"></attribute>
    </complexType>

    <complexType name="tParticipantList">
        <sequence>
            <element name="Participant" type="ipsm:tParticipant" minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tStrategyDefinition">
        <sequence>
            <element name="InteractiveInitializableEntityDefinition" type="ipsm:tInteractiveInitializableEntityDefinition"  maxOccurs="1" minOccurs="1"></element>
            <element name="TargetIntentions" type="ipsm:tIntentionResourceCapabilityRefs" maxOccurs="1" minOccurs="1"></element>
            <element name="RequiredIntentions" type="ipsm:tIntentionRefs"
                     maxOccurs="1" minOccurs="0">
            </element>
            <element name="OrganizationalCapabilities" type="ipsm:tOrganizationalCapabilityRefs" maxOccurs="1" minOccurs="0"></element>
            <element name="OperationalProcesses" type="ipsm:tResourceDrivenProcessDefinitionRefs" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tResourceDrivenProcessDefinitionRefs">
        <sequence>
            <element name="RelevantProcessDefinition" type="QName"
                     maxOccurs="unbounded" minOccurs="1">
            </element>
        </sequence>
    </complexType>

    <complexType name="tResourceDrivenProcessDefinitions">
        <sequence>
            <element name="ResourceDrivenProcessDefinition" type="ipsm:tResourceDrivenProcessDefinition"
                     maxOccurs="unbounded" minOccurs="1">
            </element>
        </sequence>
    </complexType>

    <complexType name="tMatchingRelationships">
        <sequence>
            <element name="MatchingRelationships" type="pref:tRelationshipTemplate"
                     maxOccurs="unbounded" minOccurs="1"></element>
        </sequence>
    </complexType>

    <complexType name="tInteractionList">
        <sequence>
            <element name="Interaction" type="ipsm:tInteraction"
                     minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tRelevantEntity"></complexType>

    <complexType name="tRelevantResource">
        <complexContent>
            <extension base="ipsm:tRelevantEntity">
                <sequence>
                    <element name="RelevantResource" type="QName" minOccurs="1" maxOccurs="1"></element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name="tRelevantCapability">
        <complexContent>
            <extension base="ipsm:tRelevantEntity">
                <sequence>
                    <element name="RelevantCapability" type="QName" minOccurs="1" maxOccurs="1"></element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name="tSourceResource">
        <attribute name="resourceDefinitionId" type="QName"></attribute>
        <attribute name="distance" type="integer"></attribute>
    </complexType>

    <complexType name="tSourceResources">
        <sequence>
            <element name="SourceResource" type="ipsm:tSourceResource" minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>

    <complexType name="tSourceEntities">
        <sequence>
            <element name="SourceInteractions" type="ipsm:tInteractionList" maxOccurs="1" minOccurs="0"></element>
            <element name="SourceResources" type="ipsm:tSourceResources" maxOccurs="1" minOccurs="0"></element>
        </sequence>
    </complexType>

    <complexType name="tRelevanceRelationship">
        <complexContent>
            <extension base="ipsm:tIdentifiableEntityDefinition">
                <sequence>
                    <element name="SourceEntities" type="ipsm:tSourceEntities"
                             minOccurs="1" maxOccurs="1"></element>
                    <element name="RelevantCapabilityOrResource" minOccurs="1"
                             maxOccurs="1" type="ipsm:tRelevantEntity"></element>
                </sequence>
                <attribute name="correlationCoefficient" type="double"></attribute>
            </extension>
        </complexContent>
    </complexType>

    <element name="RelevanceRelationships" type="ipsm:tRelevanceRelationships"></element>
    <complexType name="tRelevanceRelationships">
        <sequence>
            <element name="RelevanceRelationship" type="ipsm:tRelevanceRelationship" minOccurs="1" maxOccurs="unbounded"></element>
        </sequence>
    </complexType>



    <complexType name="tInteraction">
        <complexContent>
            <extension base="ipsm:tIdentifiableEntityDefinition">
                <sequence>
                    <element name="AnalyzedResource" type="ipsm:tInstanceDescriptor" maxOccurs="1"
                             minOccurs="1">
                    </element>
                    <element name="IdentifiedResource" type="ipsm:tInstanceDescriptor" maxOccurs="1"
                             minOccurs="1">
                    </element>
                    <element name="Content" type="ipsm:tContent" maxOccurs="1"
                             minOccurs="0">
                    </element>
                </sequence>
                <attribute name="digest" type="string"></attribute>
                <attribute name="action" type="string"></attribute>
                <attribute name="distance" type="integer"></attribute>
            </extension>
        </complexContent>
    </complexType>

</schema>
