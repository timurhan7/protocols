package uni_stuttgart.ipsm.protocols.integration;

import java.net.URI;
import java.util.List;


/**
 * Contains meta-data about a provider.
 * @author sungurtn
 *
 */
public interface IntegratorMetadata {
    /**
     * Get the name of the resource provider
     * @return
     */
    String getName();

    /**
     * Return the id of this specific end point, e.g., Domain Manager or Execution Environment Integrator.
     * It's a URI
     * @return
     */
    URI getTargetNamespace();

    /**
     * Returns the address of the respective URI
     * returned String should be convertable to a URI
     * Uses for REST based connection with this integrator.
     * @return
     */
    URI getUri();

    /**
     * To get the actual imported data
     * @return
     */

    List<Import> getImports();
}
