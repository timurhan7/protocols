package uni_stuttgart.ipsm.protocols.integration;

import java.io.InputStream;
import javax.xml.namespace.QName;

/**
 * Represents a deployable object
 * @author sungurtn
 *
 */
public interface Deployable {
    /**
     * @deprecated
     * Get target runtime id of the deployable
     * @return
     */
    String getTargetRuntime();

    /**
     * Returns the type of this deployable
     * @return
     */
    QName getType();

    /**
     * Returns deployable represented by this deployable
     * @return
     */
    InputStream getDeployable();
}
