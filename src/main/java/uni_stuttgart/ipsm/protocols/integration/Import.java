package uni_stuttgart.ipsm.protocols.integration;

import java.io.InputStream;
import java.net.URI;

public interface Import {
	/**
	 * Get namesapce of this import
	 * @return
	 */
	URI getNamespace();
	
	/**
	 * Returns the type of the import. For instance http://www.w3.org/2001/XMLSchema
	 * for XSD schemas
	 * @return
	 */
	URI getImportType();
	
	
	/**
	 * Returns the input stream of the actual import. This can be an XSD file or an artifact.
	 * @return
	 */
	InputStream getImport();
}
