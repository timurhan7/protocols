package uni_stuttgart.ipsm.protocols.integration.operations;

import java.net.URI;
import java.util.List;

public interface RelationshipOperationRealization extends OperationRealization {
    
    /**
     * Returns the list of relationship types that supports this operation on
     * their targets. In case a domain specific operation, this returns null and
     * all are supported.
     * 
     * @return [QName string {ns}name]
     */
    List<URI> getSupportedTargetRelationships();
    
    /**
     * Returns the list of relationship types that supports this operation on
     * their sources. In case a domain specific operation, this returns null and
     * all are supported.
     * 
     * @return [QName string {ns}name]
     */
    List<URI> getSupportedSourceRelationships();
    
    /**
     * Get supported target domains for the relationship operation
     * 
     * @return List of all namespaces that are supported by this operation as
     *         {@link URI}s
     */
    List<URI> getSupportedTargetDomains();
    
    /**
     * Get supported source domains for the relationship operation
     * 
     * @return List of all namespaces that are supported by this operation as
     *         {@link URI}s
     */
    List<URI> getSupportedSourceDomains();
    
}
