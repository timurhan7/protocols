package uni_stuttgart.ipsm.protocols.integration.operations.lifecycle;

import java.io.InputStream;
import java.net.URI;

import uni_stuttgart.ipsm.protocols.integration.operations.ResourceOperationRealization;


public abstract class BaseLifecycleResourceOperation implements ResourceOperationRealization {
	
	public static String LIFECYCLE_INTERFACE_TARGET_NAMESPACE = "http://www.uni-stuttgart.de/iaas/ipsm/operations/life-cycle/";
	public static String TARGET_NAMESPACE = "http://www.uni-stuttgart.de/iaas/ipsm/v0.1/";
	public static String XSD_FILE_NAME = "IPSM-v0.1.xsd";
	
	@Override
	public String getInterfaceName() {
		return LIFECYCLE_INTERFACE_TARGET_NAMESPACE;
	}
	
	
	@Override
	public InputStream getTypeDefinition() {
		return getClass().getResourceAsStream(XSD_FILE_NAME);
	}

	@Override
	public URI getTypeDefinitionsTargetNamespace() {
		return URI.create(TARGET_NAMESPACE);
	}

}
